﻿using Microsoft.Win32;
using Mono.Options;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.DirectoryServices;
using System.IO;
using System.Management.Automation;
using System.Security.Permissions;
using System.Security.Principal;
using Windows.UI.Xaml.Controls;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace KioskConfig
{
	/// <summary>
	/// An empty page that can be used on its own or navigated to within a Frame.
	/// </summary>
	public sealed partial class MainPage : Page
    {
        public const string kioskUsername = "CrewSightMobileKiosk";
        public const string kioskPassword = "Tr1mbl3";
        public int verbosity = 0;
		public bool error = false;


		private void Write( string text)
		{
			textBlockMessages.Text += text;
		}
		private void WriteLine( string line)
		{
			Write( line );
			Write( "\n" );
		}

		private void WriteErrorLine( string line)
		{
			WriteLine( line );
			error = true;
		}
		private bool AccountExists( string username )
		{
			bool accountExists = false;

			try
			{
				NTAccount account = new NTAccount( username );
				SecurityIdentifier id = ( SecurityIdentifier ) account.Translate( typeof( SecurityIdentifier ) );

				accountExists = id.IsAccountSid();
			}
			catch( IdentityNotMappedException )
			{
				/* Invalid user account */
			}

			return( accountExists);
		}
		public void AccountCreate()
		{
			string ps1File = @"C:\my script folder\script.ps1";
			ProcessStartInfo startInfo = new ProcessStartInfo()
			{
				FileName = "powershell.exe",
				Arguments = $"-NoProfile -ExecutionPolicy unrestricted -file \"{ps1File}\"",
				UseShellExecute = false
			};
			Process.Start( startInfo );
		}
		// 
		// Doesn't work due to PrincipalContext being hard to include.
		//public bool AccountCreate( string username, string password, string displayName, string description, bool canChangePwd, bool pwdExpires )
		//{

		//	try
		//	{
		//		PrincipalContext context = new PrincipalContext( ContextType.Machine );
		//		UserPrincipal user = new UserPrincipal( context );
		//		user.SetPassword( password );
		//		user.DisplayName = displayName;
		//		user.Name = username;
		//		user.Description = description;
		//		user.UserCannotChangePassword = canChangePwd;
		//		user.PasswordNeverExpires = pwdExpires;
		//		user.Save();


		//		//now add user to "Users" group so it displays in Control Panel
		//		GroupPrincipal group = GroupPrincipal.FindByIdentity( context, "Users" );
		//		group.Members.Add( user );
		//		group.Save();
		//
		//		return true;
		//	}
		//	catch( Exception ex )
		//	{
		//		LogMessageToFile( "error msg" + ex.Message );
		//		return false;
		//	}
		//}
		//
		// Doesn't work due to DirectoryEntry being hard to include.
		//public void AccountCreate( string username, string password )
		//{
		//	try
		//	{
		//		DirectoryEntry directoryEntry = new DirectoryEntry( "WinNT://" + Environment.MachineName + ",computer" );
		//		DirectoryEntry NewUser = directoryEntry.Children.Add( username, "user" );
		//		NewUser.Invoke( "SetPassword", new object[]{ password } );
		//		NewUser.Invoke( "Put", new object[]{ "Description", "Kiosk Mode User" } );
		//		NewUser.CommitChanges();
		//		DirectoryEntry group = directoryEntry.Children.Find( "Administrators", "group" );
		//		if( group != null ) 
		//		{ 
		//			group.Invoke( 
		//				"Add", 
		//				new object[]
		//				{ 
		//					NewUser.Path.ToString() 
		//				} 
		//			); 
		//		}
		//		WriteLine( kioskUsername + " account created successfully" );
		//	}
		//	catch( Exception ex )
		//	{
		//		WriteErrorLine( ex.Message );
		//	}
		//}
		public void SetAutoLogin( string username, string password )
		{
			//creates or opens the key provided.Be very careful while playing with 
			//windows registry.
			RegistryKey registryKey = Registry.LocalMachine.CreateSubKey( "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon" );

			if( registryKey == null )
			{
				WriteErrorLine( "writing to windows registry" );
			}
			else
			{
				//these are our hero like values here
				//simply use your RegistryKey objects SetValue method to set these keys
				registryKey.SetValue( "AutoAdminLogon", "1" );
				registryKey.SetValue( "DefaultUserName", username );
				registryKey.SetValue( "DefaultPassword", password );
			}
			//close the RegistryKey object
			registryKey.Close();
		}
		public void ClearAutoLogin()
		{
			//creates or opens the key provided.Be very careful while playing with 
			//windows registry.
			RegistryKey registryKey = Registry.LocalMachine.CreateSubKey( "SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion\\Winlogon" );

			if( registryKey == null )
			{
				WriteErrorLine( "writing to windows registry" );
			}
			else
			{
				registryKey.SetValue( "AutoAdminLogon", "0" );
			}
			//close the RegistryKey object
			registryKey.Close();
		}
		public MainPage()
        {
            this.InitializeComponent();

			bool show_help = false;
			bool start_kiosk_mode = false;
			bool stop_kiosk_mode = false;

			OptionSet option_parser = new OptionSet() {
				"Usage: kioskconfig [OPTIONS]+ message",
				"Set up kiosk mode.",
				"",
				"Options:",
				{ "v", "increase debug message verbosity", v => { if (v != null) ++verbosity; } },
				{ "k|kiosk-start",  "start kiosk mode", v => start_kiosk_mode = v != null },
				{ "s|kiosk-stop",  "stop kiosk mode", v => stop_kiosk_mode = v != null },
				{ "h|help",  "show this message and exit", v => show_help = v != null },
			};

			List<string> extra = new List<string>();
			try
			{
				string args = App.args;
				string[] arg_list = args.Split( " " );
				extra = option_parser.Parse( arg_list );
				if( !( start_kiosk_mode || stop_kiosk_mode ) )
				{
					throw new OptionException( "start or stop must be chosen", "none" );
				}
				if( stop_kiosk_mode && ( extra.Count > 0 ) )
				{
					throw new OptionException( "no extra parameter when leaving kiosk mode", "s" );
				}
				if( start_kiosk_mode && ( extra.Count == 0 ) )
				{
					throw new OptionException( "must include configuration json", "k" );
				}
			}
			catch( OptionException e )
			{
				Write( "kioskconfig: " );
				WriteErrorLine( e.Message );
				WriteLine( "Try `kioskconfig --help' for more information." );
			}

			if( show_help )
			{
				StringWriter message_writer = new StringWriter();
				option_parser.WriteOptionDescriptions( message_writer );
				WriteLine( message_writer.ToString() );
			}

			if( start_kiosk_mode )
			{

				// Store configuration data.
				string config_json = "";
				if( extra.Count > 0 )
				{
					config_json = string.Join( " ", extra.ToArray() );
					WriteLine( "Using new config: " + config_json );
				}
				Environment.SetEnvironmentVariable( "csm_config", config_json, EnvironmentVariableTarget.Machine );

				// Create Kiosk user account.
				if( !AccountExists( kioskUsername ) )
				{
					//AccountCreate( kioskUsername, kioskPassword );
					AccountCreate();
				}

				// Activate auto-login
				SetAutoLogin( kioskUsername, kioskPassword );
			}

			if( stop_kiosk_mode )
			{
				// Deactivate auto-login.
				ClearAutoLogin();
			}
		}
    }
}
