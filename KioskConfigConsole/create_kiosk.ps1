﻿$Username = "CrewSightMobileKiosk"
$PlainPassword = "Tr1mbl3"
$SecurePassword = ConvertTo-SecureString "$PlainPassword" -AsPlainText -Force
$Configuration = Get-Content -Path .\kiosk.config -TotalCount 1
[Environment]::SetEnvironmentVariable( "csm_config", "$Configuration", 'Machine')
New-LocalUser -Name $Username -Password $SecurePassword -PasswordNeverExpires  -UserMayNotChangePassword -Description "a user account used by CrewSight Mobile"
#Set-AssignedAccess -AppUserModelId Microsoft.MicrosoftEdge_8wekyb3d8bbwe!MicrosoftEdge -UserName $Username
Add-LocalGroupMember -Group "Administrators" -Member $Username
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name "AutoAdminLogon" -Value "1"
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name "DefaultPassword" -Value "$PlainPassword"
New-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Windows NT\CurrentVersion\Winlogon" -Name "DefaultUserName" -Value "$Username"
Restart-Computer
